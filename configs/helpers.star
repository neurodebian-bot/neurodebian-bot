def builder_container_name(distribution):
  return "build-{}".format(distribution)
end

def builder_image_name(distribution):
  return "localhost/build:{}".format(distribution)
end

def buildspec_builder(distribution, build_dir, tag, base_image, base_image_tag):
  return "{} {} {} {} {}".format(builder_image_name(distribution), build_dir, tag, base_image, base_image_tag)
end

def buildspec_generic(container_name, build_dir, tag):
  return "{} {} {}".format(generic_image_name(container_name), build_dir, tag)
end

def downstream_build_name(downstream_branches, branch_idx, build_architectures, arch_idx, fallback):
  if arch_idx < 0:
    return fallback
  end
  return "BUILD-{}-{}".format(downstream_branches[branch_idx].packagingBranch.replace("/", "-").upper(), build_architectures[arch_idx].upper())
end

def downstream_import_name(downstream_branches, idx):
  return "REIMPORT-{}".format(downstream_branches[idx].packagingBranch.replace("/", "-").upper())
end

def generic_container_name(container_name):
  return "{}".format(container_name)
end

def generic_image_name(container_name):
  return "localhost/{}:latest".format(container_name)
end

def primary_build_name(primary_branch, build_architectures, arch_idx, fallback):
  if arch_idx < 0:
    return fallback
  end
  return "BUILD-{}-{}".format(primary_branch.packagingBranch.replace("/", "-").upper(), build_architectures[arch_idx].upper())
end

def primary_import_name(primary_branch):
  return "IMPORT-{}".format(primary_branch.packagingBranch.replace("/", "-").upper())
end

def primary_lint_name(primary_branch):
  return "LINT-{}".format(primary_branch.packagingBranch.replace("/", "-").upper())
end
