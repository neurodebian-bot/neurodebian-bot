# This file provides versioned tags for components pulled
# from container registries or downloaded / built from Github
# upstream repositories 
#
# It is sourced by nd-bot-cli(1)

# NeuroDebian Bot version
# To be changed on each release

NEURODEBIAN_BOT_VERSION=0.5.0

# Alpine Linux container image used for provisioning and
# other supportive tasks. It provides coreutils and essential
# packages and can be upgraded rarely
ALPINE_VERSION=3.19

# Busybox for static applets
# Built from sources avaiable at: https://busybox.net/downloads
# and verified with GPG signature of Busybox lead author
BUSYBOX_VERSION=1.36.1

# Catatonit: podman-pause
# Built from source available at:
CATATONIT_VERSION=v0.2.0

# Conmon: contsiner monitor
# Built from source available at: https://github.com/containers/conmon
CONMON_VERSION=v2.1.10

# Dagu scheduler for control container
# Built from source available at: https://github.com/dagu-dev/dagu
DAGU_VERSION=v1.12.11

# fuse-overlayfs
# Built from source available at: https://github.com/containers/fuse-overlayfs
FUSEOVERLAYFS_VERSION=v1.13

# libcap
# Built from sources available at: https://git.kernel.org/pub/scm/libs/libcap/libcap.git
LIBCAP_VERSION=libcap-2.69

# libfuse
# Built fom source aavailable at: https://github.com/libfuse/libfuse
LIBFUSE_VERSION=fuse-3.16.2

# Passt/Pasta: unprivileged container networking tool
# Built from sources available at: https://git.passt.top
PASST_VERSION=2024_04_05.954589b

# Podman container tool
# Built from sources available at: https://github.com/containers/podman
PODMAN_VERSION=v4.9.4

# GVisor / runsc sandboxed container runtime
# Built from sources available at: gvisor.dev/gvisor@go
RUNSC_VERSION=9e5a99b8205044766a6b5267f6207c00fff83250

# Shadow: provides new*idmap
# Built from sources available at: https://github.com/shadow-maint/shadow
SHADOW_VERSION=4.15.1

# Yaml Templating Tool
# Built from source available at: https://github.com/carvel-dev/ytt
YTT_VERSION=v0.48.0

# Wireguard userspace implementation
# Built from sources available at: https://git.zx2c4.com/wireguard-go
WIREGUARD_GO_VERSION=12269c2761734b15625017d8565745096325392f

# Wireguard tools providing statically-built wg(1)
# Built from sources available at: https://git.zx2c4.com/wireguard-tools
WIREGUARD_TOOLS_VERSION=v1.0.20210914

# Cloudflare Warp account generator
# Built from source available at: https://github.com/ViRb3/wgcf
WGCF_VERSION=2.2.22
