The NeuroDebian Bot
-

The NeuroDebian Bot is a piece of automation managing releases of new software into NeuroDebian Project repository.

Inspired by [Debian Janitor](https://janitor.debian.net) and [conda-forge](https://conda-forge.org/), it provides well-documented way to organize flows of software releases for any Debian-based distribution.

DevOps practitioners will find the workflows at the heart of the project similar to GitHub or any other popular CI/CD service. At the same time, it requires only a single Podman host to operate!

Release Automation Workflow
--

NeuroDebian Bot facilitates faster release of new software versions into NeuroDebian by automating routine tasks like:

  * import package releases from Debian sid
  * import of new upstream releases
  * build and quality assurance checks.

Each workflow operates on set of source repositories hosted on Debian Salsa, each representing one Debian (or NeuroDebian) package.
This set is configured in `configs/bot-config.yaml`.
The bot walks through the configured repositories, performs version checks and imports, adjusts patches, tries to build and test resulting source packages and creates a "proposal repository" enclosing all the changes made.

The reviewing maintainer ("reviewer") receives the merge request where one can review changes, build logs, artifacts etc.
The reviewer can choose to: 

  * close the merge request and discard the whole run,
  * merge the request without attempts to upload resulting Debian packages into NeuroDebian (if build or test failures did occur),
  * merge the request and upload resulting Debian packages into NeuroDebian (if no failures occurred).

Once the request is merged, the packaging repository is updated with new commits across all needed branches and tags.

If failures occur, reviewer typically wants to fix them and upload working package.

After problems are fixed, reviewer opens an issue in the control repository requesting to prepare the package for upload and the bot creates another proposal run that can be either accepted or rejected.
Finally, the successful merge triggers upload of Debian packages.

This workflow combines the best approaches from prior art projects and adapts well into NeuroDebian maintainer culture.


Deployment and System Administration
--

For deployment instructions, refer to [System Administration Guide](docs/Administration.md).

Hacking
--

TODO: move technical notes into docs/Technical-Details.md
