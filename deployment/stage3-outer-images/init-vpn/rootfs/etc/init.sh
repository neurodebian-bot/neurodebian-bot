#!/bin/sh

USERSPACE=false

if ! /bin/ip link add wg0 type wireguard; then
	/bin/wireguard -u 65534:65534 wg0
	USERSPACE=true
fi

/bin/ip addr add "$WARP_ADDR4" dev wg0
/bin/ip -6 addr add "$WARP_ADDR6" dev wg0
/bin/wg setconf wg0 /run/secrets/warp-conf
/bin/ip link set wg0 up
/bin/ip rule add from "$WARP_ADDR4" lookup 80
/bin/ip -6 rule add from "$WARP_ADDR6" lookup 80
/bin/ip route add default dev wg0 table 80
/bin/ip -6 route add default dev wg0 table 80

if "$USERSPACE"; then
	exec /bin/su -s /bin/init nobody --
fi
