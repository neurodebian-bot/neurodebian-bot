#!/bin/sh

# Set TMPDIR to /scratch so it can be cleaned up
# on container start-up

export TMPDIR=/scratch
rm -rf /scratch/*

# Make cgroup writable for runsc

chown -R worker:worker /sys/fs/cgroup

# Start podman service listening on /podman/podman.sock
# NOTE: launching from /etc/inittab as unprivileged user spits weird
# UID/GID issues so let us start it here

su -s /bin/podman worker system service -t 0 unix:///podman/podman.sock &

# Make busybox init the unprivileged PID 1

exec su -s /bin/init worker